﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawerGrabber : OVRGrabber
{
    public LayerMask grabLayer;

    protected override void MoveGrabbedObject(Vector3 pos, Quaternion rot, bool forceTeleport = false)
    {
        if (m_grabbedObj == null)
        {
            return;
        }

        //if (m_grabbedObj.gameObject.layer != grabLayer)
        //{
        //    return;
        //}

        DrawerGrabbable drawerGrab = m_grabbedObj.GetComponent<DrawerGrabbable>();
        Rigidbody grabbedRigidbody = m_grabbedObj.grabbedRigidbody;
        Vector3 grabbablePosition = pos + rot * m_grabbedObjectPosOff;
        Quaternion grabbableRotation = rot * m_grabbedObjectRotOff;

        if (drawerGrab.m_restrictXMovement)
            grabbablePosition.x = grabbedRigidbody.position.x;
        if (drawerGrab.m_restrictYMovement)
            grabbablePosition.y = grabbedRigidbody.position.y;
        if (drawerGrab.m_restrictZMovement)
            grabbablePosition.z = grabbedRigidbody.position.z;

        if (forceTeleport)
        {
            grabbedRigidbody.transform.position = grabbablePosition;

            if (!drawerGrab.m_disableObjectRotation)
                grabbedRigidbody.transform.rotation = grabbableRotation;
        }
        else
        {
            grabbedRigidbody.MovePosition(grabbablePosition);

            if (!drawerGrab.m_disableObjectRotation)
                grabbedRigidbody.MoveRotation(grabbableRotation);
        }
    }
}
