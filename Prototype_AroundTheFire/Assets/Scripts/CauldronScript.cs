﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CauldronScript : MonoBehaviour
{
    public Animator animator, animatorHUD;
    public GameObject pSystem_blue, pSystem_red, pSystem_green, pSystem_fail, pSystem_success;

    public Animator endPotionAnimator;
    public GameObject potion_purple;

    public int reagentsNeeded;
    public int reagentCount;

    //Cauldron light effects
    [SerializeField] Light cauldronLight;
    [SerializeField] Color c_blue, c_red, c_green, c_fail, c_success;

    // Start is called before the first frame update
    void Start()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        //If a puzzle object is dropped in the cauldron...
        if (other.transform.CompareTag("PuzzleObject"))
        {
            //If it's any correct reagent, increment reagentCount and match cauldron color to potion.
            if (other.transform.GetComponent<PuzzleObject>().objectID == "Potion_Blue")
            {
                Destroy(other.transform.gameObject);

                reagentCount++;
                cauldronLight.color = c_blue;

                pSystem_blue.SetActive(true);
                pSystem_red.SetActive(false);
                pSystem_green.SetActive(false);

            }
            else if (other.transform.GetComponent<PuzzleObject>().objectID == "Potion_Red")
            {
                Destroy(other.transform.gameObject);

                reagentCount++;
                cauldronLight.color = c_red;

                pSystem_blue.SetActive(false);
                pSystem_red.SetActive(true);
                pSystem_green.SetActive(false);

            }
            else if (other.transform.GetComponent<PuzzleObject>().objectID == "Potion_Green")
            {
                Destroy(other.transform.gameObject);

                reagentCount++;
                cauldronLight.color = c_green;

                pSystem_blue.SetActive(false);
                pSystem_red.SetActive(false);
                pSystem_green.SetActive(true);
            }
            else
            {
                //Level failed!
                Destroy(other.transform.gameObject);

                pSystem_blue.SetActive(false);
                pSystem_red.SetActive(false);
                pSystem_green.SetActive(false);
                pSystem_fail.SetActive(true);
                pSystem_success.SetActive(false);

                cauldronLight.color = c_fail;
                cauldronLight.intensity = 1f;

                animator.SetTrigger("CauldronFlip");

                StartCoroutine(PotionComplete());

                //Disable trigger after potion failed.
                GetComponent<BoxCollider>().enabled = false;
            }

            //If it's the first reagent, start shaking and activate slow particles, increase light.
            if (reagentCount == 1)
            {
                animator.SetTrigger("StartShaking");
                animator.speed = 0.5f;

                cauldronLight.intensity += 0.75f;
            }
            //If it's the second reagent, speed up shaking, increase light.
            if (reagentCount == 2)
            {
                animator.speed = 1f;

                cauldronLight.intensity += 0.75f;
            }
            //If it's the third reagent, speed up shaking and activate final particles, change color to purple.
            if (reagentCount == reagentsNeeded)
            {
                animator.speed = 1.5f;

                cauldronLight.color = c_success;
                cauldronLight.intensity += 0.75f;

                pSystem_blue.SetActive(false);
                pSystem_red.SetActive(false);
                pSystem_green.SetActive(false);
                pSystem_fail.SetActive(false);
                pSystem_success.SetActive(true);

                StartCoroutine(PotionComplete());

                //Disable trigger after potion is complete.
                GetComponent<BoxCollider>().enabled = false;
            }

        }
    }

    IEnumerator PotionComplete()
    {
        if (reagentCount == 2)
        {
            //Set Potion_Purple as active and play animation.
            potion_purple.SetActive(true);
            endPotionAnimator.SetTrigger("PotionRise");
        }

        yield return new WaitForSecondsRealtime(3.5f);

        //Triggers HUD black FadeIn when potion is complete.
        animatorHUD.SetTrigger("FadeIn");

        //Wait a bit then reload the scene.
        yield return new WaitForSecondsRealtime(5f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
