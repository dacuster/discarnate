﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorGrabbable : DrawerGrabbable
{
    public Transform handler;

    public override void GrabEnd(Vector3 linearVelocity, Vector3 angularVelocity)
    {
        base.GrabEnd(Vector3.zero, Vector3.zero);

        transform.position = handler.position;
        transform.rotation = handler.rotation;
    }

    private void Update()
    {
        if(Vector3.Distance(handler.position, transform.position) > 0.3f)
        {
            grabbedBy.ForceRelease(this);
        }
    }
}
